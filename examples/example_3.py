from thresult import Result, Ok, Err


def _div3(x: float, y: float) -> float:
    z: float = x / y
    return z


def _div2(x: float, y: float) -> float:
    return _div3(x, y)


def _div1(x: float, y: float) -> float:
    return _div2(x, y)


@Result[float, Exception]
def div(x: float, y: float) -> float:
    z: float = _div1(x, y)
    return z


def ex0():
    # Ok
    r: Result = div(1.0, 10.0)
    v: float | Exception = r.unwrap_value()
    print(f'Ok: {v!r}')

    # Err
    r: Result = div(1.0, 0.0)
    v: float | Exception = r.unwrap_value()
    print(f'Err {v!r}')


def ex1():
    z: float

    # ok
    with div(1.0, 2.0) as z:
        print('0a', z)
    
    print('0b', z)

    # error
    try:
        with div(1.0, 0.0) as z:
            print('1a', z)
    except ZeroDivisionError as e:
        print('1a error', e)
    
    print('1b', z)


if __name__ == '__main__':
    ex0()
    ex1()
