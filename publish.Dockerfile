FROM python:3.11-alpine as base
RUN apk add --no-cache build-base linux-headers bsd-compat-headers musl-dev openssl-dev libffi-dev git curl rust cargo
RUN /usr/local/bin/python -m pip install --upgrade pip

FROM base as build
COPY . /code
WORKDIR /code
ENV PATH="${PATH}:/root/.poetry/bin"
RUN pip install poetry
RUN poetry config virtualenvs.create false
CMD poetry publish --build --username $PYPI_USERNAME --password $PYPI_PASSWORD
